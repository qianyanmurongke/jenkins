package com.example.demo;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.example.demo.disruptor.DisruptorMqService;
import com.taobao.api.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootTest
class DemoMybatisApplicationTests {

    @Test
    void contextLoads() {
        //获取应用访问凭证accessToken
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");
        OapiGettokenRequest req = new OapiGettokenRequest();
        req.setAppkey("dinggaq7utflndp2ug8e");
        req.setAppsecret("OHG-iB21EpNHzhJrr2NO8livqazn9u4PFievZ1tfSMN0t43cH3FIcLuhhg84rpDy");
        req.setHttpMethod("GET");
        OapiGettokenResponse rsp = null;
        try {
            rsp = client.execute(req);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        System.out.println(rsp.getBody());
        JSONObject token=JSONUtil.parseObj(rsp.getBody());
        System.out.println(token);
        //获取部门
//        OapiV2UserListRequest req1 = new OapiV2UserListRequest();
//        req1.setDeptId(1L);
//        req1.setCursor(0L);
//        req1.setSize(10L);
//        String qq1=HttpRequest.post("https://oapi.dingtalk.com/topapi/v2/department/listsub?access_token="+token.get("access_token")).body("").execute().body();
//        System.out.println(qq1);
//        //调用通讯录服务端API-获取部门用户详情接口，获取企业员工在钉钉组织架构中的userId值信息
//        DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/list");
//        OapiV2UserListRequest req1 = new OapiV2UserListRequest();
//        req1.setDeptId(746849195L);
//        req1.setCursor(0L);
//        req1.setSize(10L);
//        Map<String,Object> map=new HashMap<>();
//        map.put("dept_id",746849195L);
//        map.put("cursor",0);
//        map.put("size",10);
//         String qq=HttpRequest.post("https://oapi.dingtalk.com/topapi/v2/user/list?access_token="+token.get("access_token")).body(JSONUtil.toJsonStr(map)).execute().body();
//        System.out.println(qq);

        Map<String,Object> map1=new HashMap<>();
        map1.put("userid","manager1632");
        map1.put("device_name","东门考勤机");
        map1.put("device_id","abc123456");
        map1.put("user_check_time",System.currentTimeMillis());
        String qq1=HttpRequest.post("https://oapi.dingtalk.com/topapi/attendance/record/upload?access_token="+token.get("access_token")).body(JSONUtil.toJsonStr(map1)).execute().body();
        System.out.println(qq1);
        //调用通讯录服务端API-获取部门用户详情接口，获取企业员工在钉钉组织架构中的userId值信息
        DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/list");
        OapiV2UserListRequest req2 = new OapiV2UserListRequest();
        req2.setDeptId(746849195L);
        req2.setCursor(0L);
        req2.setSize(10L);
        Map<String,Object> map=new HashMap<>();
        map.put("dept_id",746849195L);
        map.put("cursor",0);
        map.put("size",10);
         String qq=HttpRequest.post("https://oapi.dingtalk.com/topapi/v2/user/list?access_token="+token.get("access_token")).body(JSONUtil.toJsonStr(req2)).execute().body();
        System.out.println(qq);

//        Map<String,Object> map1=new HashMap<>();
//        map1.put("userid","manager1632");
//        map1.put("device_name","东门考勤机");
//        map1.put("device_id","abc123456");
//        map1.put("user_check_time",System.currentTimeMillis());
//        String qq1=HttpRequest.post("https://oapi.dingtalk.com/topapi/attendance/record/upload?access_token="+token.get("access_token")).body(JSONUtil.toJsonStr(map1)).execute().body();
//        System.out.println(qq1);
    }
    @Autowired
    private DisruptorMqService disruptorMqService;
    /**
     * 项目内部使用Disruptor做消息队列
     * @throws Exception
     */
    @Test
    public void sayHelloMqTest() throws Exception{
        disruptorMqService.sayHelloMq("test1","消息到了，Hello world!");
        log.info("消息队列已发送完毕");
        //这里停止2000ms是为了确定是处理消息是异步的
        Thread.sleep(2000);
    }
}
