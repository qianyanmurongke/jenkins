package com.example.demo;

import com.tencentcloudapi.ckafka.v20190819.CkafkaClient;
import com.tencentcloudapi.ckafka.v20190819.models.*;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;

public class SendMessage {
    public static void main(String[] args) {
        // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
        // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取

        Credential  credential=new Credential("AKID661oU1Rmz2xkObUExb4Qn6FBuTrFXirE","KqfNxQmiZJ2dAaAc4jv66sQmyxmU1JMB");
        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
//        httpProfile.setEndpoint("ckafka.tencentcloudapi.com");
        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        ClientProfile clientProfile = new ClientProfile();
//        clientProfile.setHttpProfile(httpProfile);
        // 实例化要请求产品的client对象,clientProfile是可选的
        CkafkaClient client = new CkafkaClient(credential, "ap-shanghai", clientProfile);
        // 实例化一个请求对象,每个接口都会对应一个request对象
        SendMessageRequest req = new SendMessageRequest();
        req.setDataHubId("datahub-y3gkv9o2");


        BatchContent[] batchContents1 = new BatchContent[2];
        BatchContent batchContent1 = new BatchContent();
        batchContent1.setBody("test1");
        batchContents1[0] = batchContent1;


        BatchContent batchContent2 = new BatchContent();
        batchContent2.setBody("test2");
        batchContents1[1] = batchContent2;


        req.setMessage(batchContents1);


        // 返回的resp是一个SendMessageResponse的实例，与请求对象对应
        SendMessageResponse resp = null;
        try {
            resp = client.SendMessage(req);
        } catch (TencentCloudSDKException e) {
            throw new RuntimeException(e);
        }
        // 输出json格式的字符串回包
        System.out.println(SendMessageResponse.toJsonString(resp));

    }
}
