package com.example.demo.dto;

import lombok.Data;

/**
 * @author wangfei
 * @Description
 * @date 2022/7/26 16:19
 */
@Data
public class SysCenterDto {

    /**
    * 省份名称
    */
    private String provincename;

}
