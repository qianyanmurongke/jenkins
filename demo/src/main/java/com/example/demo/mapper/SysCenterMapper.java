package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.domain.SysCenter;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysCenterMapper extends BaseMapper<SysCenter> {
}
