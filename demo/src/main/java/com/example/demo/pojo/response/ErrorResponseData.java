package com.example.demo.pojo.response;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
* @description: 失败响应结果
* @author:wangfei
* @date:2022/7/26 16:15
*/
@EqualsAndHashCode(callSuper = true)
@Data
public class ErrorResponseData extends ResponseData {

    /**
     * 异常的具体类名称
     */
    private String exceptionClazz;

    ErrorResponseData(String message) {
        super(false, DEFAULT_ERROR_CODE, message, null);
    }

    public ErrorResponseData(Integer code, String message) {
        super(false, code, message, null);
    }

    ErrorResponseData(Integer code, String message, Object object) {
        super(false, code, message, object);
    }
}
