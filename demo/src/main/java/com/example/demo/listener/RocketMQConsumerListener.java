package com.example.demo.listener;

import cn.hutool.core.date.DateUtil;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RocketMQMessageListener(consumerGroup = "test1",topic = "test1")
public class RocketMQConsumerListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.out.println("消费消息："+s+"----"+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss.SSS"));
    }
}
