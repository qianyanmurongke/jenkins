package com.example.demo.disruptor;

/**
* @description 实现类-生产者
* @author wangfei
* @email 15850205347@163.com
* @date 2023/11/17 16:19
*/

public interface DisruptorMqService {
    /**
     * 消息
     * @param message
     */
    void sayHelloMq(String topic,String message);
}
