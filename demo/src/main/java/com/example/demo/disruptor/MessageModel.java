package com.example.demo.disruptor;

import lombok.Data;

@Data
public class MessageModel {
    private String message;

    private String topic;
}
