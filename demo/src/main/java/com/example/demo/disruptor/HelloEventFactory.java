package com.example.demo.disruptor;

import com.lmax.disruptor.EventFactory;


/**
* @description 构造EventFactory
* @author wangfei
* @email 15850205347@163.com
* @date 2023/11/17 15:35
*/

public class HelloEventFactory implements EventFactory<MessageModel> {
    @Override
    public MessageModel newInstance() {
        return new MessageModel();
    }
}
