package com.example.demo.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangfei
 * @Description
 * @date 2022/7/26 16:21
 */
@Data
@TableName("sys_center")
public class SysCenter implements Serializable {
    private  static  final  long servailVersionUID=1L;

    /**
    *  主键id
    */
    @TableField("ID")
    private String id;

    /**
     *  日照中心名称
     */
    @TableField("NAME")
    private String NAME;

    /**
     *  负责人
     */
    @TableField("CHARGEPERSONAL")
    private String CHARGEPERSONAL;
    /**
     *  负责人手机
     */
    @TableField("PHONE")
    private String PHONE;

    /**
     *  省份编码
     */
    @TableField("PROVINCECOE")
    private String PROVINCECOE;

    /**
     *  省份名称
     */
    @TableField("PROVINCENAME")
    private String PROVINCENAME;

    /**
     *  城市编码
     */
    @TableField("CITYCODE")
    private String CITYCODE;

    /**
     *  城市名称
     */
    @TableField("CITYNAME")
    private String CITYNAME;

    /**
     *  地区编码
     */
    @TableField("AREACODE")
    private String AREACODE;

    /**
     * 地区名称
     */
    @TableField("AREANAME")
    private String AREANAME;

    /**
     *  详细地址
     */
    @TableField("ADDRESS")
    private String ADDRESS;

    /**
     *  营业时间
     */
    @TableField("BUSINESSHOUR")
    private String BUSINESSHOUR;

    /**
     *  药店id
     */
    @TableField("PHRAMACYID")
    private String PHRAMACYID;

    /**
     *  药店名称
     */
    @TableField("PHRAMACYNAME")
    private String PHRAMACYNAME;

    /**
     *  集团id
     */
    @TableField("GROUPID")
    private String GROUPID;

    /**
     *  集团id
     */
    @TableField("GROUPNAME")
    private String GROUPNAME;

    /**
     *  类型（0：单体；1：连锁）
     */
    @TableField("TYPE")
    private String TYPE;

    /**
     *  状态（0：停用；1：启用）
     */
    @TableField("STATUS")
    private String STATUS;

    /**
     *  是否删除
     */
    @TableField("DELETESTAT")
    private String DELETESTAT;

    /**
     *  创建时间
     */
    @TableField("CREATETIME")
    private String CREATETIME;

    /**
     *  修改时间
     */
    @TableField("UPDATETIME")
    private String UPDATETIME;



}
