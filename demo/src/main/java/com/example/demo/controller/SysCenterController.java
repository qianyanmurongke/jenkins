package com.example.demo.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.domain.SysCenter;
import com.example.demo.dto.SysCenterDto;
import com.example.demo.pojo.response.ResponseData;
import com.example.demo.pojo.response.SuccessResponseData;
import com.example.demo.service.SysCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangfei
 * @Description
 * @date 2022/7/26 15:31
 */
@RestController
@RequestMapping("/center")
public class SysCenterController {
    @Autowired
    private SysCenterService sysCenterService;

    @PostMapping("/page")
    public ResponseData page(@RequestBody SysCenterDto dto){
        Page<SysCenter> page=new Page<>(10,1);
        QueryWrapper<SysCenter> wrapper=new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(dto.getProvincename())) {
            wrapper.like("PROVINCENAME", dto.getProvincename());
        }
        Page<SysCenter> sysCenterPage= sysCenterService.page(page,wrapper);
        return new SuccessResponseData(sysCenterPage);
    }

}
