package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    Logger logger = LoggerFactory.getLogger(TestController.class);
    @GetMapping("test")
    public String test(){
        System.out.println("111111111111");
        logger.info("测试log");
        for(int i = 0; i < 30; i++){
            logger.error("测试log. id={}; name=Ryan-{};", i, i);
        }
        return "hello11";
    }
}
