package com.example.demo.controller;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/mq")
public class MQcontroller {
    @Resource
    private RocketMQTemplate rocketMQTemplate;
    @RequestMapping("/send")
    public void send() {
        rocketMQTemplate.convertAndSend("test1", "test-message");
    }


    /**
     * 发送异步消息
     */
    @RequestMapping("/testASyncSend")
    public void testASyncSend(){
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：消息内容
        //参数三：回调
        rocketMQTemplate.asyncSend("test1", "异步消息测试", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println(sendResult);
            }
            @Override
            public void onException(Throwable throwable) {
                System.out.println("消息发送异常");
                throwable.printStackTrace();
            }
        });
    }

    /**
     * 发送延时消息
     */
    @RequestMapping("/testDelaySend")
    public void testDelaySend(){

        Map<String,Object> orderMap = new HashMap<>();
        orderMap.put("orderNumber","1357890");
        System.out.println("发送消息："+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss.SSS"));
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：Message<?>
        //参数三：消息发送超时时间
        //参数四：delayLevel 延时level  messageDelayLevel=1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
        rocketMQTemplate.syncSend("test1", MessageBuilder.withPayload(JSONObject.toJSONString(orderMap)).build(),3000,3);
    }

}
