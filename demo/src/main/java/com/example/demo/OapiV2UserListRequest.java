package com.example.demo;

import lombok.Data;

@Data
public class OapiV2UserListRequest {
    private Long deptId;

    private Long cursor;

    private Long size;
}
