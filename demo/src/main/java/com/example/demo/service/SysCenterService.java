package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.domain.SysCenter;

public interface SysCenterService extends IService<SysCenter> {
}
