package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.domain.SysCenter;
import com.example.demo.mapper.SysCenterMapper;
import com.example.demo.service.SysCenterService;
import org.springframework.stereotype.Service;

/**
 * @author wangfei
 * @Description
 * @date 2022/7/26 17:02
 */
@Service
public class SysCenterServiceImpl extends ServiceImpl<SysCenterMapper, SysCenter> implements SysCenterService {
}
